#coding: utf-8

from dotenv import load_dotenv
from os import getenv

import requests
import dotenv
from os import getenv, environ
from time import time

from flask import Flask, request, make_response
from re import search

from mysql.connector import connect

from datetime import datetime

class DbInputStream:

    def __init__(self, host: str, port: int, user: str, pswd: str, database: str = "EnsembllFlow") -> None:
        self.host = host
        self.port = port
        self.user = user
        self.pswd = pswd
        self.database = database
        print(self.pswd)

    def connectToDb(self):
        return connect(host=self.host, port=self.port,
                                 user=self.user, password=self.pswd, database=self.database)

    def disconnect(self, connection):
        connection.close()

    def getCursor(self, connection):
        return connection.cursor()

    def write(self, query: str) -> None:
        connection = self.connectToDb()
        csr = self.getCursor(connection)
        csr.execute(query)
        connection.commit()
        csr.close()
        self.disconnect(connection)

    def read(self, query: str) -> list:
        connection = self.connectToDb()
        csr = self.getCursor(connection)
        csr.execute(query)
        a = csr.fetchall()
        csr.close()
        self.disconnect(connection)
        return a

    def close_conn(self) -> None:
        self.connector.close()

GET_BOOKING_FOR_DATETIME_AND_PLATE = """SELECT * FROM Bookings WHERE plate="{plate}" AND startTS<="{datetime}" AND endTS>="{datetime}" """
BASE_URL = "https://cerberus.parkki.io/v1.0.0"
GET_TOKEN = "/auth/login"
GET_REFRESH_TOKEN = "/auth/request_access_token"
POST_OPEN_ACCESS = "/accesses/{id}/open"

class APIinterface:

    def __init__(self, mail, pswd) -> None:
        self.dotenv_file = dotenv.find_dotenv('../app/.env')

        self.mail = mail
        self.pswd = pswd

        self.auth_token: dict = None
        self.refresh_token: dict = None
        self.user: dict = None
        self.contract:dict = None

    def fetch_auth_token(self, mail: str, pswd: str) -> bool:
        """Get authentification tokens from the API for current session to make further requests

        Args:
            mail (str): Email used for login
            pswd (str): Password used for login

        Returns:
            bool: True if connection was successfull otherwise False
        """

        r = requests.post(BASE_URL + GET_TOKEN,
                          data={"email": mail, "password": pswd})
        if r.status_code == 200:
            data = r.json()
            self.write_tokens(data["access_token"],
                              data["refresh_token"], data["user"])
            return True
        else:
            raise ConnectionRefusedError(
                f"Fetch token responded with a {r.status_code}")

    def refresh_auth_token(self) -> bool:
        r = requests.get(BASE_URL + GET_REFRESH_TOKEN, headers={"Authorization": "Bearer " + self.refresh_token["token"]})
        if r.status_code == 200:
            data = r.json()
            self.write_tokens(data["access_token"],
                              data["refresh_token"])
            return True
        else:
            print(ConnectionRefusedError(
                f"Fetch token responded with a {r.status_code}, fetching new tokens"))
            self.fetch_auth_token(self.mail, self.pswd)

    def should_refresh(self) -> bool:
        if int(self.auth_token["expires_at"]) < int(time() + 86400):
            return True
        return False

    def get_api(self, url: str, path_parameters: dict = None, query_payload: dict = None) -> requests.Response:
        """Make a GET to the API

        Args:
            url (str): Ressource's URL to retreive
            query_payload (dict, optional): Query data to filter the GET. Defaults to None.

        Raises:
            response.status_code: HTTP Code if the API sent anything else than 200

        Returns:
            json: API Response when request is 200
        """
        if self.should_refresh():
            self.refresh_auth_token()

        if (path_parameters != None):
            response = requests.get(
                BASE_URL + url.format(**path_parameters), headers={"Authorization": "Bearer " + self.auth_token["token"]}, params=query_payload)
        else:
            response = requests.get(
                BASE_URL + url, headers={"Authorization": "Bearer " + self.auth_token["token"]}, params=query_payload)
        return response

    def post_api(self, url: str, payload: dict = None, path_payload: dict = None) -> requests.Response:
        """Make a POST to the API

        Args:
            url (str): Ressource's URL to send
            payload (dict): Data to send

        Raises:
            response.status_code: HTTP Code if the API sent anything else than 200

        Returns:
            json: API Response when request is 200
        """
        if self.should_refresh():
            self.refresh_auth_token()
        
        if path_payload != None:
            r = requests.post(
                BASE_URL + url.format(**path_payload), headers={"Authorization": "Bearer " + self.auth_token["token"]}, json=payload)
        else:
            r = requests.post(
                BASE_URL + url, headers={"Authorization": "Bearer " + self.auth_token["token"]}, json=payload)
        return r

    def read_tokens(self):
        self.auth_token = {
            'token': getenv("API_USER_TOKEN"),
            'expires_at': getenv("API_USER_TOKEN_EXPIRATION")
        }

        self.refresh_token = {
            'token': getenv("API_USER_REFRESH_TOKEN"),
            'expires_at': getenv("API_USER_REFRESH_TOKEN_EXPIRATION")
        }

    def write_tokens(self, access_token, refresh_token, user=None):
        self.auth_token = access_token
        self.refresh_token = refresh_token
        if user != None:
            self.user = user

        environ["API_USER_TOKEN"] = str(access_token["token"])
        environ["API_USER_TOKEN_EXPIRATION"] = str(access_token["expires_at"])

        environ["API_USER_REFRESH_TOKEN"] = str(refresh_token["token"])
        environ["API_USER_REFRESH_TOKEN_EXPIRATION"] = str(
            refresh_token["expires_at"])

        dotenv.set_key(self.dotenv_file, "API_USER_TOKEN",
                       environ["API_USER_TOKEN"])
        dotenv.set_key(self.dotenv_file, "API_USER_TOKEN_EXPIRATION",
                       environ["API_USER_TOKEN_EXPIRATION"])

        dotenv.set_key(self.dotenv_file, "API_USER_REFRESH_TOKEN",
                       environ["API_USER_REFRESH_TOKEN"])
        dotenv.set_key(self.dotenv_file, "API_USER_REFRESH_TOKEN_EXPIRATION",
                       environ["API_USER_REFRESH_TOKEN_EXPIRATION"])

        print("[INFO] Write tokens OK")

load_dotenv('../app/.env')
db_flow = DbInputStream('127.0.0.1', int(getenv("DB_PORT")), getenv("DB_FLOW_WRITER_ID"), getenv("DB_FLOW_WRITER_PSWD"), database=getenv("DB_FLOW_NAME"))
db = DbInputStream('127.0.0.1', int(getenv("DB_PORT")), getenv("DB_ID"), getenv("DB_PSWD"), database='Ensembll')
api_log = getenv("API_PARKKI_LOG")
api_token = getenv("API_PARKKI_TOKEN")
api = APIinterface(api_log, api_token)
api.read_tokens()

def handleOpening(plate: str, checkBooking: bool = True):
    if checkBooking and len(db.read(GET_BOOKING_FOR_DATETIME_AND_PLATE.format(plate=plate, datetime=datetime.now()))) < 1:
        print('No booking')
        return

    # Open
    r1 = api.post_api(POST_OPEN_ACCESS, path_payload={'id': '63e5027875729c00bf3ec7b9'}, payload={'location': {'lat': 50.6510382372813, 'lng': 2.96874134678398}})
    r2 = api.post_api(POST_OPEN_ACCESS, path_payload={'id': '63e5028d75729c00bf3ec7bc'}, payload={'location': {'lat': 50.65103823728132, 'lng': 2.9687413467839767}})

    if r1.status_code == 204 and r2.status_code == 204:
        print(f'OPEN {plate} OK')
    else:
        print(f'OPEN {plate} API ERROR')

handleOpening('AA-123-AA')

app = Flask(__name__)

@app.route("/plate", methods=['POST'])
def retreive_plate():
    payload = request.get_data().decode()
    print(payload)
    mac = search(".*<macAddress>(.*)</macAddress>.*", payload).group(1)
    plate = search(".*<licensePlate>([a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2})</licensePlate>.*", payload)
    record = datetime.fromisoformat(search(".*<dateTime>(.*)</dateTime>.*", payload).group(1)).strftime("%Y-%m-%d %H:%M:%S")
    #if match('^[A-Z]{2}-[0-9]{3}-[A-Z]{2}$', plate) or match('^[0-9]{4}-[A-Z]{2}-[0-9]{2}$', plate):

    if plate is None:
        plate = search(".*<licensePlate>([0-9]{4}[a-zA-Z]{2}[0-9]{2})</licensePlate>.*", payload)
        if plate is None:
            print(f"Read plate is invalid ({plate})")
            return make_response("OK", 200)
        else:
            plate = plate.group(1)
            plate = plate[:4]+"-"+plate[4:6]+"-"+plate[6:]

    else:
        plate = plate.group(1)
        plate = plate[:2]+"-"+plate[2:5]+"-"+plate[5:]


    if mac == "24:0f:9b:73:58:a3":
        handleOpening(plate)
        db_flow.write(f"""INSERT INTO FlowIn (plate, record) VALUES ("{plate}", "{record}") """)
    elif mac == "24:0f:9b:73:58:b1":
        handleOpening(plate, checkBooking=False)
        db_flow.write(f"""INSERT INTO FlowOut (plate, record) VALUES ("{plate}", "{record}") """)
    else:
        record = "Adresse MAC non reconnue"

    return make_response("OK", 200)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=False)
